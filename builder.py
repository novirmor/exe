#!/usr/bin/env python3
import os
import shutil
import argparse
import subprocess


build_dir = "build"
binary_dir = "build/bin"
output_dir = "output"
project = "exe"

space = " "


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--clean",
        help="clean working directory", action="store_true")
    parser.add_argument(
        "-d", "--deps", action="store_true",
        help="add dependency repositories")
    parser.add_argument(
        "-b", "--build",
        help="build realese binary", action="store_true")
    parser.add_argument(
        "-t", "--tests",
        help="start specified tests [lint , all]")
    parser.add_argument(
        "-r", "--run", action="store_true",
        help="run ")
    parser.add_argument(
        "-bs", "--buildsdk", action="store_true",
        help="build sdk")
    args = parser.parse_args()
    if args.clean:
        clean()
    if args.buildsdk:
        buildsdk()
    if args.deps:
        add_deps()
    if args.build:
        build()
    if args.run:
        run()
    if args.tests:
        do_tests(args.tests)


def do_tests(a):
    pass
    # prepare_build_dir()
    # subprocess.run(
    #     ["cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .. -G Ninja"],
    #     shell=True)
    # if (a == "lint" or a == "all"):
    #     subprocess.run(["run-clang-tidy"], shell=True)
    # return_to_project_dir()


def add_deps():
    cmd = '{cmd="conan remote add " $1 " " $2; system(cmd) }'
    subprocess.run(
        ["awk -F '# " + cmd + " conanrepos.txt"],
        shell=True)


def prepare_build_dir():
    if not os.path.exists(build_dir):
        os.makedirs(build_dir)
    os.chdir(build_dir)


def return_to_project_dir():
    os.chdir("..")


def buildsdk():
    subprocess.run(
        ["docker image build . --tag " + project + "-sdk:latest"],
        shell=True)


def build():
    prepare_build_dir()
    subprocess.run(
        ["conan install .. -s compiler=gcc -s compiler.libcxx=libstdc++11 --build=missing"],
        shell=True)
    subprocess.run(["conan build .."], shell=True)
    return_to_project_dir()


def run():
    subprocess.run([binary_dir+"/"+project], shell=True)


def clean():
    if os.path.exists(build_dir):
        shutil.rmtree(build_dir)
    if os.path.exists(binary_dir):
        shutil.rmtree(binary_dir)
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)


main()
