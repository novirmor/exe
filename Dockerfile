FROM debian:stretch

RUN \
      apt-get update && \
      apt-get install -y --no-install-recommends \
      build-essential \
      dumb-init \
      ca-certificates \
      python3 \
      python3-pip \
      python3-setuptools \
      cmake \
      dumb-init \
      g++ \
      gawk \
      python3 &&\
      apt-get autoremove && \
      apt-get autoclean

RUN pip3 install conan

RUN groupadd --gid 1000 ca && \
      useradd --no-log-init --create-home --uid 1000 --gid 1000 ca
USER ca
WORKDIR /home/ca

COPY ./conanrepos.txt .
RUN awk -F "#" '{cmd="conan remote add " $1 " " $2; system(cmd) }' conanrepos.txt

COPY ./conanfile.py .
RUN conan install . -s compiler=gcc -s compiler.libcxx=libstdc++11 --build=missing

ENTRYPOINT ["dumb-init", "--"]
CMD ["bash"]