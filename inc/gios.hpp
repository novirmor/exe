#pragma once
#include <fmt/core.h>
#include <sqlite_orm/sqlite_orm.h>
#include <list>
#include <nlohmann/json.hpp>
#include <string>

namespace orm = sqlite_orm;
using namespace nlohmann;

namespace gios {
const std::string StationsUrl =
    "http://api.gios.gov.pl/pjp-api/rest/station/findAll";
const std::string SensorsUrl =
    "http://api.gios.gov.pl/pjp-api/rest/station/sensors/";

struct station {
  int id = -1;
  std::string name;
  std::string latitude;
  std::string longitude;
  std::string sensors;
};

void print(station s);

void to_json(json& j, const station& s);

void from_json(const json& j, station& s);

void insertStationsInDB();

inline auto initDB() {
  auto tmp = orm::make_storage(
      "gios.sqlite",
      orm::make_table("stations",
                      orm::make_column("id", &station::id, orm::autoincrement(),
                                       orm::primary_key()),
                      orm::make_column("name", &station::name),
                      orm::make_column("latitude", &station::latitude),
                      orm::make_column("longitude", &station::longitude),
                      orm::make_column("sensors", &station::sensors)));
  tmp.sync_schema();
  return tmp;
}
}  // namespace gios
