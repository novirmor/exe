#include "gios.hpp"
#include <cpr/cpr.h>
#include <fmt/core.h>

namespace gios {
void to_json(json& j, const station& s) {
  j = json{{"gegrLon", s.longitude},
           {"gegrLat", s.latitude},
           {"stationName", s.name}};
}

void from_json(const json& j, station& s) {
  j.at("stationName").get_to(s.name);
  j.at("gegrLat").get_to(s.latitude);
  j.at("gegrLon").get_to(s.longitude);
}
void print(station s) {
  std::string sensors;
  fmt::print("name: {}, lattitude: {}, logtitude: {}, sensors: {}\n", s.name,
             s.latitude, s.longitude, s.sensors);
}

void insertStationsInDB() {
  auto db = initDB();
  auto r = cpr::Get(cpr::Url{StationsUrl});
  fmt::print("Status code: {:d}\n", r.status_code);
  auto j = json::parse(r.text);
  for (auto& i : j) {
    station s = i;
    auto response = cpr::Get(
        cpr::Url{fmt::format(SensorsUrl + "{}", static_cast<int>(i["id"]))});
    auto response_json = json::parse(response.text);
    for (auto& j : response_json) {
      int k = j["id"];
      s.sensors += "#" + std::to_string(k);
    }
    db.insert(s);
  }
}
}  // namespace gios
