from conans import ConanFile, CMake


class MylibConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    requires = ("cpr/1.3.0@zimmerk/stable",
                "fmt/5.2.1@bincrafters/stable",
                "sqlite_orm/1.1@bincrafters/stable",
                "jsonformoderncpp/3.4.0@vthiery/stable")

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
